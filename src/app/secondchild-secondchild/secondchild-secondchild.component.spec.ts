import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondchildSecondchildComponent } from './secondchild-secondchild.component';

describe('SecondchildSecondchildComponent', () => {
  let component: SecondchildSecondchildComponent;
  let fixture: ComponentFixture<SecondchildSecondchildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondchildSecondchildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondchildSecondchildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
