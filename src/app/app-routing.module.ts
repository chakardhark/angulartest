import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { App2Module } from './app2/app2.module';
import { App2RoutingModule } from './app2/app2.routing';
import { FirstchildComponent } from './firstchild/firstchild.component';
import { TestGuard } from './guards/test.guard';
import { TestchildGuard } from './guards/testchild.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { SecondchildFirstchildComponent } from './secondchild-firstchild/secondchild-firstchild.component';
import { SecondchildSecondchildComponent } from './secondchild-secondchild/secondchild-secondchild.component';
import { SecondchildComponent } from './secondchild/secondchild.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [TestGuard],
    children: [
      {
        path: 'first',
        component: FirstchildComponent
      },
      {
        path: 'second',
        component: SecondchildComponent,
        canActivate: [TestchildGuard],
        children: [
          {
            path: 'first',
            component: SecondchildFirstchildComponent
          },
          {
            path: 'second',
            component: SecondchildSecondchildComponent
          },
          {
            path: '',
            component: SecondchildFirstchildComponent
          }
        ]
      },
      {
        path: '',
        component: FirstchildComponent
      }
    ]
  },
  {
    path: 'app2',
    loadChildren: './app2/app2.module#App2Module'
  },
  {
    path: '',
    component: LoginComponent
  },
  {
    path: '**',
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
