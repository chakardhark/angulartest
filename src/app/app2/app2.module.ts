import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { App2RoutingModule } from './app2.routing';
import { App2Component } from './app2/app2.component';
import { HomeComponent } from './home/home.component';
import { FirstComponent } from './first/first.component';


@NgModule({
  declarations: [App2Component, HomeComponent, FirstComponent],
  imports: [
    CommonModule,
    App2RoutingModule
  ],
  bootstrap: [App2Component]
})
export class App2Module { }
