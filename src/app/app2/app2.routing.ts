import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { App2Component } from './app2/app2.component';
import { FirstComponent } from './first/first.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
    {
        path: '',
        component: App2Component
    },
    {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: 'first',
                component: FirstComponent
            },
            {
                path: '',
                component: FirstComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class App2RoutingModule { }
