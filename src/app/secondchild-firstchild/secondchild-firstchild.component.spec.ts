import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondchildFirstchildComponent } from './secondchild-firstchild.component';

describe('SecondchildFirstchildComponent', () => {
  let component: SecondchildFirstchildComponent;
  let fixture: ComponentFixture<SecondchildFirstchildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondchildFirstchildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondchildFirstchildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
