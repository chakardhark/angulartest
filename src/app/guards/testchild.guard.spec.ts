import { TestBed, async, inject } from '@angular/core/testing';

import { TestchildGuard } from './testchild.guard';

describe('TestchildGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestchildGuard]
    });
  });

  it('should ...', inject([TestchildGuard], (guard: TestchildGuard) => {
    expect(guard).toBeTruthy();
  }));
});
